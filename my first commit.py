#!/usr/bin/env python
# coding: utf-8

# In[ ]:


# %load mymodule.py
#!/usr/bin/env python

# In[2]:


mytuple = ("apple", "banana", "cherry")
myit = iter(mytuple)
print(next(myit))
print(next(myit))
print(next(myit))


# In[3]:


mytuple = ("apple", "banana", "cherry")

for x in mytuple:
  print(x)


# In[4]:


mystr = "banana"

for x in mystr:
  print(x)


# In[5]:


def myfunc():
  x = 100
  print(x)

myfunc()


# In[6]:


def myfunc():
  x = 100
  def myinnerfunc():
    print(x)
  myinnerfunc()

myfunc()


# In[7]:


x = 100

def myfunc():
  x = 200
  print(x)

myfunc()

print(x)


# In[10]:


def greeting(name):
  print("Hello, " + name)

import mymodule
mymodule.greeting("Bill")


# In[9]:


import mymodule

mymodule.greeting("Bill")


# In[ ]:


def greeting(name):
  print("Hello, " + name)

person1 = {
  "name": "Bill",
  "age": 63,
  "country": "USA"
}


# In[2]:


get_ipython().run_line_magic('run', 'mymodule.py')


# In[6]:


# %load mymodule.py
#!/usr/bin/env python

# In[2]:


mytuple = ("apple", "banana", "cherry")
myit = iter(mytuple)
print(next(myit))
print(next(myit))
print(next(myit))


# In[3]:


mytuple = ("apple", "banana", "cherry")

for x in mytuple:
  print(x)


# In[4]:


mystr = "banana"

for x in mystr:
  print(x)


# In[5]:


def myfunc():
  x = 100
  print(x)

myfunc()


# In[6]:


def myfunc():
  x = 100
  def myinnerfunc():
    print(x)
  myinnerfunc()

myfunc()


# In[7]:


x = 100

def myfunc():
  x = 200
  print(x)

myfunc()

print(x)


# In[10]:


def greeting(name):
  print("Hello, " + name)

import mymodule
mymodule.greeting("Bill")


# In[9]:


import mymodule

mymodule.greeting("Bill")


# In[ ]:


def greeting(name):
  print("Hello, " + name)

person1 = {
  "name": "Bill",
  "age": 63,
  "country": "USA"
}


# In[ ]:




